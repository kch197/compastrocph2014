FUNCTION wrap, x
  return, ((x+32) mod 64)-32
END

match='000002'
u=[0.0,1.0,1.0]
do_plot=0
xr=[-32,32]
;jv=2 & yr=[0,2]
jv=7 & yr=[0.01,1]
color
;loadct,3
window,xs=640,ys=320

close,1
openr,1,'write_amr.log'
file=''
n=intarr(3)
t=0.0
level=0
f1=fltarr(16,16) & f2=f1 & f3=f1
new=0
first=1
while not eof(1) and t lt 66 do begin
  readf,1,file
  readf,1,n,t,level
  f=fltarr(n[0],n[1],n[2])
  if first then begin
    first=0
    x=4.0*(findgen(n[0])+0.5-0.5*n[0])
    plot,xr,yr,/nodata,xst=3,yst=3
  end
  print,file,t
  ;if strpos(file,match) gt 0 then begin
    close,2
    openr,2,file
    a=assoc(2,fltarr(20,20))
    f=a[jv*20+n[2]/2]
    title=file+string(t,format='(f6.3)')
    if level eq 1 then begin
      f1=f[2:17,2:17]
      if do_plot eq 1 then begin
        plot,x+t*u[0],f[*,9],psy=-1,xr=xr,yr=yr,xst=3,yst=3
        ;oplot,x+t*u[0],f[*,9],psy=-1
        oplot,x[2:17]+t*u[0],f[2:17,9],psy=2
        new=0
      end else begin
        new=1
      end
    end else if level eq 2 then begin
      f2=f[2:17,2:17]
      if do_plot eq 1 then begin
        ;plot,x/2.,f[*,n[1]/2],psy=-4,yr=yr,yst=3
        oplot,wrap(x/2.+t*u[1]),f[*,9],psy=-4,color=thecolor('cyan')
        oplot,wrap(x/2.+t*u[1]),f[*,8],psy=-4,color=thecolor('blue')
        oplot,wrap(x[2:17]/2.+t*u[1]),f[2:17,n[1]/2],psy=2
        wait,0.05
      end
      new=0
    end else begin
      f3=f[2:17,2:17]
      if do_plot eq 1 then begin
        ;plot,x/4.,f[*,n[1]/2],psy=-4,yr=yr,yst=3
        oplot,x/4.+t*u[2],f[*,9],psy=-6,color=thecolor('orange')
        oplot,x/4.+t*u[2],f[*,8],psy=-6,color=thecolor('red')
        oplot,x[2:17]/4.+t*u[2],f[2:17,n[1]/2],psy=2
        wait,0.05
      end
      new=0
    end
    if new eq 1 then begin
      imsz,320
      image,f1,zr=[0,1],/grid,0,0
      imsz,160
      image,f2,zr=[0,1],/grid,75+t*u[1]*160./64.,75
      imsz,80
      image,f3,zr=[0,1],/grid,110+t*u[2]*160./64.,110
      wait,0.1
    end
    wait,0.0
    ;plot,x,f[*,n[1]/2,n[2]/2],title=title,xst=3
    ;oplot,x[2:n[0]-3],f[2:n[0]-3,n[1]/2,n[2]/2],psy=6
    ;oplot,x[0:1],f[0:1,n[1]/2,n[2]/2],psy=1
    ;oplot,x[n[0]-2:n[0]-1],f[n[0]-2:n[0]-1,n[1]/2,n[2]/2],psy=1
    ;print,file,n,iv,t,min(f,max=max),max,format='(a,2x,i2,2x,3i4,g11.3,3x,2g12.3)'
    ;print,f[n[0]/2-2:n[0]/2+2,n[1]/2,n[2]/2]
    ;print,'continuing ...'
  ;end
  close,2
end

END