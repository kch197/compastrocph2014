!*******************************************************************************
PROGRAM coords_example
  USE mpi_coords
  implicit none
  integer, dimension(3):: gn, n_mpi
!-------------------------------------------------------------------------------
  call init_mpi

  n_mpi = 1; gn = (/32,32,1/)
  call cart_create_mpi (n_mpi, gn)
  call finalize_cart

  n_mpi = 1; gn = (/100,25,1/)
  call cart_create_mpi (n_mpi, gn)
  call end_mpi
END PROGRAM coords_example
