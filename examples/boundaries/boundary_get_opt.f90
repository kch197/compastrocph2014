!*******************************************************************************
! This version uses an optimized approach based on MPI_Get.  A loop identical
! to the one filling the boundary zones is first used to store our values, which
! we need to send, into a snd buffer where they can be picked up with MPI_Get.
! Correspondingly, we get that buffer int rcv, and unpack it in the original loop.
!*******************************************************************************
SUBROUTINE boundary (f)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: f
  integer :: idim, ix, iy, iz, lb(3), ub(3), offset(3), n
  !
  ! Allocate send and receive buffers and open a window into the send one
  !
  n = max((m(1)%ub-m(1)%lb+1)*(m(2)%ub-m(2)%lb+1)*m(3)%nghost, &
          (m(2)%ub-m(2)%lb+1)*(m(3)%ub-m(3)%lb+1)*m(1)%nghost, &
          (m(3)%ub-m(3)%lb+1)*(m(1)%ub-m(1)%lb+1)*m(2)%nghost)
  allocate (snd(n), rcv(n))
  call win_create_mpi (win, snd, n)
  !
  do idim=1,3
    !
    ! If there is only a single mesh point in this direction, it is passive. Cycle loop
    !
    if (m(idim)%n==1) cycle
    !
    ! Data has to be fetched a box length away
    !
    offset = 0
    offset(idim) = m(idim)%n
    !
    ! Lower boundary
    !
    lb = m%lb
    ub = m%ub
    ub(idim) = -1 ! only update boundary zones in lower part
    nx = ub(1)-lb(1)+1
    nxy = nx*(ub(2)-lb(2)+1)
    !$omp parallel do private(ix,iy,iz,n)
    do iz=lb(3),ub(3)
    do iy=lb(2),ub(2)
    do ix=lb(1),ub(1)
      n = 1 + ix-lb(1) + (iy-lb(2))*nx + (iz-lb(3))*nxy
      snd(n) = f(ix+offset(1), iy+offset(2), iz+offset(3))
    enddo
    enddo
    enddo
    call win_fence_mpi (win)
    call get_reals_mpi (win, rcv, n, 0, mpi_dn(idim))
    call win_fence_mpi (win)
    !$omp parallel do private(ix,iy,iz,n)
    do iz=lb(3),ub(3)
    do iy=lb(2),ub(2)
    do ix=lb(1),ub(1)
      n = 1 + ix-lb(1) + (iy-lb(2))*nx + (iz-lb(3))*nxy
      f(ix,iy,iz) = rcv(n)
    enddo
    enddo
    enddo
    !
    ! Lower boundary
    !
    ...
  end do
  !
  call win_free_mpi (win)
  deallocate (snd, rcv)
  !
END SUBROUTINE boundary
