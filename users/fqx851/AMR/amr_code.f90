!*******************************************************************************
! The task of the main program is mainly to initialize MPI, and then let the
! dispatcher create the initial set of patches and start its work.
!*******************************************************************************
PROGRAM amr_code
  USE mpi_base,     only: init_mpi, end_mpi
  USE dispatch_mod, only: dispatch
  USE amr_mod,      only: init_amr
  USE mesh_mod,     only: init_mesh
  USE io
  implicit none
!...............................................................................
 do_trace = .true.                                                             ! trace execution
 !do_debug = .true.                                                             ! debug printout
  call init_mpi
  call dispatch
  call end_mpi
END PROGRAM

