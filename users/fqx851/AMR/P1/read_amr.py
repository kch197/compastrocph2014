""" Based on a Mayavi example
"""
import time
import numpy as np
from scipy import special
from numpy import fromfile, float32, prod, transpose
from matplotlib.pyplot import imshow, show, draw
import matplotlib.pyplot as pl
import matplotlib.animation as animation
from os import path

def load(i):
    global shape
    filename='id={:0=7d}_{:0=5d}.dat'.format(taskid,i+1)
    print(filename)
    fd=open(filename,'rb')
    dd=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    ds=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Ux=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Uy=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Uz=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Bx=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    By=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Bz=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    return (dd,ds,Ux,Uy,Uz,Bx,By,Bz)

def save(fig, i):
    filename = path.join(folder,'plot{:0=5d}.png'.format(i))
    fig.savefig(filename)
    print "saving figure "+filename

n=64
shape=(n,n)
i = 0
i_n = 200
f_save = 49
taskid=0
folder = "plots"

fig=pl.figure()
_,_,_,_,_,_,_,Bz = load(i)
im=pl.imshow(Bz,origin='lower')
save(fig,i)
def update_fig(*args):
    global i
    i = i + 1
    if(i>=i_n): return im
    _,_,_,_,_,_,_,Bz = load(i)
    im.set_data(Bz)
    if(i%f_save==0): 
        save(fig,i)
    return im
ani = animation.FuncAnimation(fig, update_fig, interval=20, blit=False)
pl.show()


