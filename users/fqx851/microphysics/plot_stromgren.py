import numpy as np
import matplotlib.pyplot as pyplot

idx_H = 0 #index H2
idx_Hj = 1 #index O+
idx_O = 2 #index H2
idx_Oj = 3 #index O+
idx_E = 4 #index free electrons

#species to plot
species = [["H", idx_H, "red"],
           ["H+",idx_Hj, "green"],
           ["O", idx_O, "blue"],
           ["O+",idx_Oj, "purple"]] 
#list of handles to plots for labelling
handles = []

data = np.fromfile("stromgren_sphere_O_alt_cross_section.dat", sep=' ')
if(len(data)%(300*5)!=0):
  raise Exception("Error reading file. Number of entries is not a whole multiple of grid points x species.")
ntime = len(data)/(300*5)
data.shape = (ntime,300,5)
pyplot.yscale(u'log')
pyplot.title(u"Strömgren sphere including oxygen")
pyplot.xlabel(u"radius/box length")
pyplot.ylabel(u"number density $cm^{-3}$")
for i in xrange(ntime):
  for (lbl_txt, id, color) in species:
    handle=pyplot.plot(np.arange(300.)/300., data[i,:,id], color=color, label=lbl_txt)
    if(i==0): handles.append(handle[0])

pyplot.grid(True)
pyplot.legend(handles=handles)
pyplot.show()