PROGRAM pi
  implicit none
  integer, parameter :: n = 10000000
  real, parameter :: ref_pi = 3.14159265359
  integer k
  real calc_pi

  calc_pi=0
  do k=0,n
    calc_pi = calc_pi + (-1.0)**k/(2.0*k+1.0)
  end do
  calc_pi = calc_pi*4
  print*, "Reference pi", ref_pi
  print*, "Calculated pi", calc_pi
END PROGRAM
