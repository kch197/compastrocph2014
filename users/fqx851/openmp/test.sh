#!/bin/bash

gfortran -fopenmp $1.f90 -o $1.x
echo "with 1 thread"
export num_threads=1
export OMP_NUM_THREADS=$num_threads; time ./$1.x
echo "with 4 threads"
export num_threads=4
export OMP_NUM_THREADS=$num_threads; time ./$1.x