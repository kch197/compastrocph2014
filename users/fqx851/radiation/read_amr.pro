match='000003'
jv=7
color
loadct,3
window,xs=640,ys=640

close,1
openr,1,'write_amr.log'
file=''
n=intarr(3)
t=0.0
level=0
f1=fltarr(16,16,16) & f2=f1 & f3=f1
new=0
while not eof(1) and t lt 66 do begin
  readf,1,file
  readf,1,n,t,level
  f=fltarr(n[0],n[1],n[2])
  x=findgen(n[0])+0.5
  ;print,file,t
  ;if strpos(file,match) gt 0 then begin
    close,2
    openr,2,file
    a=assoc(2,fltarr(20,20))
    f=a[7*20+n[2]/2]
        title=file+string(t,format='(f6.3)')
        if level eq 1 then begin
          f1=f[2:17,2:17]
          new=1
        end else if level eq 2 then begin
          f2=f[2:17,2:17]
          new=0
        end else begin
          f3=f[2:17,2:17]
          new=0
        end
        if new eq 1 then begin
         imsz,640
         image,f1,zr=[0,1],/grid,0,0
         imsz,320
         image,f2,zr=[0,1],/grid,150,150
         imsz,160
         image,f3,zr=[0,1],/grid,220,220
        wait,0.1
        end
        ;plot,x,f[*,n[1]/2,n[2]/2],title=title,xst=3
        ;oplot,x[2:n[0]-3],f[2:n[0]-3,n[1]/2,n[2]/2],psy=6
        ;oplot,x[0:1],f[0:1,n[1]/2,n[2]/2],psy=1
        ;oplot,x[n[0]-2:n[0]-1],f[n[0]-2:n[0]-1,n[1]/2,n[2]/2],psy=1
        ;print,file,n,iv,t,min(f,max=max),max,format='(a,2x,i2,2x,3i4,g11.3,3x,2g12.3)'
        ;print,f[n[0]/2-2:n[0]/2+2,n[1]/2,n[2]/2]
        ;print,'continuing ...'
  ;end
  close,2
end

END