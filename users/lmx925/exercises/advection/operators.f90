MODULE operators
  USE mesh,       only: m
  USE boundaries, only: boundary
  use omp_lib  
  implicit none
CONTAINS
!*******************************************************************************
! Divergence operator -- uses 4th order centered finite difference
!*******************************************************************************
FUNCTION div(vx,vy,vz)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: div
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: vx,vy,vz
  integer :: idim, ix, iy, iz
  real    :: ax, bx, ay, by, az, bz,seconds
  
  !
  !$omp parallel default(none) &
  !$omp   private(ix,iy,iz, ax,ay,az,bx,by,bz) shared(div,m,vx,vy,vz,seconds)
  !
  


  !$omp single
  seconds = omp_get_wtime ( ) - seconds
  !$omp end single
  ax = 8. / (12.*m(1)%d); bx = -1. / (12.*m(1)%d)
  ay = 8. / (12.*m(2)%d); by = -1. / (12.*m(2)%d)
  az = 8. / (12.*m(3)%d); bz = -1. / (12.*m(3)%d)

  !
  if (m(1)%n > 1) then ! x-derivative only non-zero iff x-dimension larger than 1
    !$omp do
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      div(ix,iy,iz) = ax*(vx(ix+1,iy,iz) - vx(ix-1,iy,iz)) &
                    + bx*(vx(ix+2,iy,iz) - vx(ix-2,iy,iz))                     ! d(vx)/dx
    end do
    end do
    end do
    !$omp enddo nowait
  endif
  if (m(2)%n > 1) then
    !$omp do
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      div(ix,iy,iz) = div(ix,iy,iz) + &
                    + ay*(vy(ix,iy+1,iz) - vy(ix,iy-1,iz)) &
                    + by*(vy(ix,iy+2,iz) - vy(ix,iy-2,iz))                     ! d(vy)/dy
    end do
    end do
    end do
    !$omp enddo nowait
  endif
  if (m(3)%n > 1) then
    !$omp do
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      div(ix,iy,iz) = div(ix,iy,iz) + &
                    + az*(vz(ix,iy,iz+1) - vz(ix,iy,iz-1)) &
                    + bz*(vz(ix,iy,iz+2) - vz(ix,iy,iz-2))                     ! d(vz)/dz
    end do
    end do
    end do
    !$omp enddo nowait
  endif
  !$omp single
  seconds = omp_get_wtime ( ) - seconds
  !$omp end single
  !$omp end parallel
  
  call boundary (div)
  print *, "time elapsed ", seconds
  !
END FUNCTION div
END MODULE operators
