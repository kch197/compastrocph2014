!*******************************************************************************
!
! Example OpenMP program.  Try compiling this with OpenMP option:
!
!   ifort -openmp sumpi.f90 -o sumpi.x
!   gfortran -fopenmp sumpi.f90 -o sumpi.x
!
! Then run it with one core, and with -- for openmp -- 4 cores::
!
!   export OMP_NUM_THREADS=1; ./sumpi.x      # with bash shell
!   export OMP_NUM_THREADS=4; ./sumpi.x      # with bash shell
!
!   setenv OMP_NUM_THREADS 1; ./openmp1.x      # with tcsh shell
!   setenv OMP_NUM_THREADS 4; ./openmp1.x      # with tcsh shell
!
! You probably don't get the same answer.   Correct the code!
!
!*******************************************************************************
PROGRAM sumpi
  implicit none
  integer, parameter:: n=1000000
  real, dimension(n):: a
  real:: s,sp
  integer:: i

  !$omp parallel default(none) &
  !$omp   private(i,sp) shared(a,s)
  sp = 0
  !$omp do
  do i=0,n-1
    a(i+1) = (-1.0)**i /(2.0*i+1.0)
  end do
  !$omp enddo
  
  !$omp single 
  s = 0.
  !$omp end single
  
  !$omp do
  do i=1,n
     sp = sp + a(i)
  enddo
  !$omp enddo
  !$omp critical
     s = s + sp
  !$omp end critical
  !$omp end parallel

  print *,"Approximation for PI is",4.0*s
  print *,"Absolute error",4.0*s-3.14159265358979323846
  print *,"Relative error", (4.0*s-3.14159265358979323846)/3.14159265358979323846

END PROGRAM sumpi
