window,0,xs=512,ys=512
window,1,xs=512,ys=512
device,decompose=0
loadct,4 
for i=0,20 do begin
  filename='id=0000000_'+string(i,form='(i5.5)')+'.dat'
  close,1
  openr,1,filename
  a=assoc(1,fltarr(64,64,64))
  rho=a[*,*,*,0]
  vx =a[*,*,*,1]/rho
  vy =a[*,*,*,2]/rho
  vz =a[*,*,*,3]/rho
  phi=a[*,*,*,8]
  pgas =a[*,*,*,9]
  ;print,filename
  print, 'Density :', i, min(rho), mean(rho), max(rho)
  wset, 0
  tvscl,rebin(alog(rho[*,*,32]),512,512)
  print, 'Pgas    :', i, min(pgas), mean(pgas), max(pgas)
  wset, 1
  tvscl,rebin(alog(pgas[*,*,32]),512,512)
  wait,0.2
end
close,/all

END
