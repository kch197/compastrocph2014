MODULE operators
  USE mesh,       only: m
  USE boundaries, only: boundary
  implicit none
CONTAINS

!*******************************************************************************
! Divergence and curl operator -- uses 4th order centered finite difference
!*******************************************************************************
FUNCTION div(vx,vy,vz)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: div
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: vx,vy,vz
  !
  div = ddx(vx) + ddy(vy) + ddz(vz)
  call boundary (div)
END FUNCTION div

!*******************************************************************************
SUBROUTINE curl(ax,ay,az,bx,by,bz)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: ax,ay,az
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: bx,by,bz
  !
  bx = ddy(az) - ddz(ay)
  by = ddz(ax) - ddx(az)
  bz = ddx(ay) - ddy(ax)
  call boundary(bx)
  call boundary(by)
  call boundary(bz)
END SUBROUTINE curl

!*******************************************************************************
FUNCTION laplace (f)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: laplace
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: f
  integer :: idim, ix, iy, iz
  real    :: a, b
  !

  !$omp parallel private(iz)
  !$omp do
  do iz=0,m(3)%n-1
    laplace(:,:,iz) = 0.0
  end do

  if (m(1)%n > 1) then 
    a = 1./(m(1)%d)**2 ; b = 2./(m(1)%d)**2
    !$omp do
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      laplace(ix,iy,iz) = &
      laplace(ix,iy,iz) +  a*(f(ix+1,iy,iz) + f(ix-1,iy,iz)) - b*f(ix,iy,iz)
    end do
    end do
    end do
    !$omp end do nowait
  endif

  if (m(2)%n > 1) then 
    a = 1./(m(2)%d)**2 ; b = 2./(m(2)%d)**2
    !$omp do
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      laplace(ix,iy,iz) = &
      laplace(ix,iy,iz) +  a*(f(ix,iy+1,iz) + f(ix,iy-1,iz)) - b*f(ix,iy,iz)
    end do
    end do
    end do
    !$omp end do nowait
  endif

  if (m(3)%n > 1) then 
    a = 1./(m(3)%d)**2 ; b = 2./(m(3)%d)**2
    !$omp do
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      laplace(ix,iy,iz) = &
      laplace(ix,iy,iz) +  a*(f(ix,iy,iz+1) + f(ix,iy,iz-1)) - b*f(ix,iy,iz)
    end do
    end do
    end do
    !$omp end do nowait
  endif
  !$omp end parallel
END FUNCTION laplace

!*******************************************************************************
! Upwind divergence operator -- uses 3rd order upwind finite difference
!*******************************************************************************
FUNCTION div_upwind(vx,vy,vz,ux,uy,uz)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: div_upwind
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: vx,vy,vz  ! Quantity to take upwind from
  real :: ux,uy,uz                                                              ! Signal velocity - selects forward/backward
                                                                                ! derivative according to sign of u[xyz]
  div_upwind = ddx_upwind(vx,ux) + ddy_upwind(vy,uy) + ddz_upwind(vz,uz)
  call boundary (div_upwind)
END FUNCTION div_upwind
!*******************************************************************************
FUNCTION ddx(vx)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: ddx
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: vx
  integer :: idim, ix, iy, iz
  real    :: ax, bx
  !
  ax = 8. / (12.*m(1)%d); bx = -1. / (12.*m(1)%d)
  if (m(1)%n > 1) then ! x-derivative only non-zero iff x-dimension larger than 1
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      ddx(ix,iy,iz) = ax*(vx(ix+1,iy,iz) - vx(ix-1,iy,iz)) &
                    + bx*(vx(ix+2,iy,iz) - vx(ix-2,iy,iz))                     ! d(vx)/dx
    end do
    end do
    end do
  else
    ddx = 0.0
  endif
  call scalar_stats (vx, 'vx')
  call scalar_stats (ddx, 'ddx')
END FUNCTION ddx

!*******************************************************************************
FUNCTION ddx_upwind(vx,ux)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: ddx_upwind
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: vx
  integer :: idim, ix, iy, iz
  real    :: ab,bb,cb,db,eb,ux
  real    :: af,bf,cf,df,ef
  ! Backwards derivative coefficients
  ab = 0.
  bb =  2. / (6.*m(1)%d)
  cb =  3. / (6.*m(1)%d)
  db = -6. / (6.*m(1)%d)
  eb =  1. / (6.*m(1)%d)
  ! Forward derivative coefficients
  af = -1. / (6.*m(1)%d)
  bf =  6. / (6.*m(1)%d)
  cf = -3. / (6.*m(1)%d)
  df = -2. / (6.*m(1)%d)
  ef = 0.
  !
  ddx_upwind = 0.
  if (m(1)%n > 1) then ! x-derivative only non-zero iff x-dimension larger than 1
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      if (ux > 0) then  ! backward derivative
        ddx_upwind(ix,iy,iz) = bb*vx(ix+1,iy,iz) + cb*vx(ix  ,iy,iz) &
                             + db*vx(ix-1,iy,iz) + eb*vx(ix-2,iy,iz)          ! d(vx)/dx
      else                        ! forward derivative

        ddx_upwind(ix,iy,iz) = df*vx(ix-1,iy,iz) + cf*vx(ix,iy  ,iz) &
                             + bf*vx(ix+1,iy,iz) + af*vx(ix+2,iy,iz)          ! d(vy)/dy

        

      endif
    end do
    end do
    end do
  else
    ddx_upwind = 0.0
  endif
END FUNCTION ddx_upwind
!*******************************************************************************
FUNCTION ddy(vy)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: ddy
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: vy
  integer :: idim, ix, iy, iz
  real    :: ay, by
  !
  ay = 8. / (12.*m(2)%d); by = -1. / (12.*m(2)%d)
  !
  if (m(2)%n > 1) then
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      ddy(ix,iy,iz) = ay*(vy(ix,iy+1,iz) - vy(ix,iy-1,iz)) &
                    + by*(vy(ix,iy+2,iz) - vy(ix,iy-2,iz))                     ! d(vy)/dy
    end do
    end do
    end do
  else
    ddy = 0.0
  endif
  call scalar_stats (vy, 'vy')
  call scalar_stats (ddy, 'ddy')
END FUNCTION ddy

!*******************************************************************************
FUNCTION ddy_upwind(vy,uy)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: ddy_upwind
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: vy
  integer :: idim, ix, iy, iz
  real    :: ab,bb,cb,db,eb,uy
  real    :: af,bf,cf,df,ef
  ! Backwards derivative coefficients
  ab = 0.
  bb =  2. / (6.*m(2)%d)
  cb =  3. / (6.*m(2)%d)
  db = -6. / (6.*m(2)%d)
  eb =  1. / (6.*m(2)%d)
  ! Forward derivative coefficients
  af = -1. / (6.*m(2)%d)
  bf =  6. / (6.*m(2)%d)
  cf = -3. / (6.*m(2)%d)
  df = -2. / (6.*m(2)%d)
  ef = 0.
  !
  ddy_upwind = 0.
  if (m(2)%n > 1) then ! x-derivative only non-zero iff x-dimension larger than 1
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      if (uy > 0) then ! backward derivative
        ddy_upwind(ix,iy,iz) = bb*vy(ix,iy+1,iz) + cb*vy(ix,iy  ,iz) &
                             + db*vy(ix,iy-1,iz) + eb*vy(ix,iy-2,iz)          ! d(vy)/dy
      else                        ! forward derivative


        ddy_upwind(ix,iy,iz) = df*vy(ix,iy-1,iz) + cf*vy(ix,iy  ,iz) &
                             + bf*vy(ix,iy+1,iz) + af*vy(ix,iy+2,iz)          ! d(vy)/dy


      endif
    end do
    end do
    end do
  else
    ddy_upwind = 0.0
  endif
END FUNCTION ddy_upwind
!*******************************************************************************
FUNCTION ddz(vz)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: ddz
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: vz
  integer :: idim, ix, iy, iz
  real    :: az, bz
  !
  az = 8. / (12.*m(3)%d); bz = -1. / (12.*m(3)%d)
  !
  if (m(3)%n > 1) then
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      ddz(ix,iy,iz) = az*(vz(ix,iy,iz+1) - vz(ix,iy,iz-1)) &
                    + bz*(vz(ix,iy,iz+2) - vz(ix,iy,iz-2))                     ! d(vz)/dz
    end do
    end do
    end do
  else
    ddz = 0.0
  endif
  call scalar_stats (vz, 'vz')
  call scalar_stats (ddz, 'ddz')
END FUNCTION ddz

!*******************************************************************************
SUBROUTINE scalar_stats (f, c)
  USE mpi_reduce
  USE io, only: do_debug
  implicit none
  real, dimension(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub):: f
  real:: mn, av, mx
  real, dimension(:,:,:), allocatable:: interior
  character(len=*):: c
!...............................................................................
  if (.not.do_debug) return
  allocate (interior(m(1)%n, m(2)%n, m(3)%n))
  interior = f(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1)                                ! skip ghost zones
  mn = minval(interior);            call min_real_mpi(mn)                       ! global min
  av = sum(interior)/product(m%gn); call sum_real_mpi(av)                       ! global average
  mx = maxval(interior);            call max_real_mpi(mx)                       ! global max
  if (master) print '(1x,a8,3x,a,1p,3e11.3)', c, ' min, aver, max :', mn, av, mx
  deallocate (interior)
END SUBROUTINE scalar_stats

!*******************************************************************************
FUNCTION ddz_upwind(vz,uz)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: ddz_upwind
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: vz
  integer :: idim, ix, iy, iz
  real    :: ab,bb,cb,db,eb
  real    :: af,bf,cf,df,ef,uz
  ! Backwards derivative coefficients
  ab = 0.
  bb =  2. / (6.*m(3)%d)
  cb =  3. / (6.*m(3)%d)
  db = -6. / (6.*m(3)%d)
  eb =  1. / (6.*m(3)%d)
  ! Forward derivative coefficients
  af = -1. / (6.*m(3)%d)
  bf =  6. / (6.*m(3)%d)
  cf = -3. / (6.*m(3)%d)
  df = -2. / (6.*m(3)%d)
  ef = 0.
  !
  ddz_upwind = 0.0 
  if (m(3)%n > 1) then ! x-derivative only non-zero iff x-dimension larger than 1
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      if (uz > 0) then  ! backward derivative
        ddz_upwind(ix,iy,iz) = bb*vz(ix,iy,iz+1) + cb*vz(ix,iy,iz  ) &
                             + db*vz(ix,iy,iz-1) + eb*vz(ix,iy,iz-2)          ! d(vz)/dz
      else                        ! forward derivative


        ddz_upwind(ix,iy,iz) = df*vz(ix,iy,iz-1) + cf*vz(ix,iy  ,iz) &
                             + bf*vz(ix,iy,iz+1) + af*vz(ix,iy,iz+2)          ! d(vy)/dy


      endif
    end do
    end do
    end do
  else
    ddz_upwind = 0.0
  endif
END FUNCTION ddz_upwind
!*******************************************************************************
END MODULE operators
