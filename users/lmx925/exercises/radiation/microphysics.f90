MODULE microphysics

CONTAINS

!************************************************************************
SUBROUTINE get_temperature(S, rho, n, T)
integer, intent(in) :: n(3)
real, intent(in)  :: S(n(1),n(2),n(3))      ! Entropy per volume
real, intent(in)  :: rho(n(1),n(2),n(3)) 	! Density
real, intent(out) :: T(n(1),n(2),n(3)) 		! Temperature
real :: gamma, mu, kb, mH
 
gamma = 1.4
mu    = 1.0
kb    = 1.0
mH    = 1.0

T = mu/kb * rho**(gamma-1.0) * exp(S*(gamma-1.0)*mH*mu/rho)

END SUBROUTINE get_temperature
!************************************************************************


!************************************************************************
SUBROUTINE get_delta_tau(alpha, rho, dz, n, dtau)
integer, intent(in) :: n(3)
real, intent(in)  :: alpha(n(1),n(2),n(3))  ! Opacity field
real, intent(in)  :: rho(n(1),n(2),n(3))    ! Density field
real, intent(in)  :: dz					    ! Delta distance
real, intent(out) :: dtau(n(1),n(2),n(3))   ! Delta optical depty
! alpha = opacity, rho 

dtau = alpha * rho * dz

END SUBROUTINE get_delta_tau
!************************************************************************
END MODULE