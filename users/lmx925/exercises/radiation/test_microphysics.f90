PROGRAM test_microphysics

USE microphysics

integer, parameter :: n(3) = 2
real, dimension(n(1),n(2),n(3)) :: S, rho, T
real, dimension(n(1),n(2),n(3)) :: alpha, dtau

S = 2.0
T = 0.0
rho = 0.4


call get_temperature(S, rho, n, T)

print *, "Temperature is: ", T

alpha = 0.1
rho   = 0.4
dz    = 0.01
dtau  = 0.0

call get_delta_tau(alpha, rho, dz, n, dtau)

print *, "Change in optical depth is: ", dtau

END PROGRAM