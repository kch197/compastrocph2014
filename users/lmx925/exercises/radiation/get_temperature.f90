SUBROUTINE get_temperature(S,rho,T)
real, intent(in) :: S, rho
real, intent(out):: T
real :: gamma,mu,kb
 
gamma = 1.4
mu    = 1.0
kb    = 1.0 
! mu/kb assumed to be 1 in these units. 

T = mu/kb rho**(gamma-1.0) * exp(S*(gamma-1.0))

END SUBROUTINE get_temperature