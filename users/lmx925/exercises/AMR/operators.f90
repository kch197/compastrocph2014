MODULE operators
  USE mesh_mod,   only: m
  USE boundaries, only: boundary
  implicit none
CONTAINS

!*******************************************************************************
! Divergence and curl operator -- uses 4th order centered finite difference
!*******************************************************************************
FUNCTION div(vx,vy,vz)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: div
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: vx,vy,vz
  integer:: iz
  !
  !$omp parallel do private(iz)
  do iz=m(3)%lb,m(3)%ub
    div(:,:,iz) = 0.0
  end do
  call ddx_add (vx, div, +1.0)
  call ddy_add (vy, div, +1.0)
  call ddz_add (vz, div, +1.0)
  call boundary (div)
END FUNCTION div

!*******************************************************************************
SUBROUTINE curl(ax,ay,az,bx,by,bz)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: ax,ay,az
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: bx,by,bz
  integer:: iz
  !
  !$omp parallel do private(iz)
  do iz=m(3)%lb,m(3)%ub
    bx(:,:,iz) = 0.0
    by(:,:,iz) = 0.0
    bz(:,:,iz) = 0.0
  end do
  call ddy_add (az, bx, -1.0); call ddz_add (ay, bx, +1.0)
  call ddz_add (ax, by, -1.0); call ddx_add (az, by, +1.0)
  call ddx_add (ay, bz, -1.0); call ddy_add (ax, bz, +1.0)
END SUBROUTINE curl

!*******************************************************************************
FUNCTION laplace (f)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: laplace
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: f
  integer :: idim, ix, iy, iz
  real    :: a, b
  !

  !$omp parallel private(iz)
  !$omp do
  do iz=m(3)%lb,m(3)%ub
    laplace(:,:,iz) = 0.0
  end do

  if (m(1)%n > 1) then 
    a = 1./(m(1)%d)**2 ; b = 2./(m(1)%d)**2
    !$omp do
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      laplace(ix,iy,iz) = &
      laplace(ix,iy,iz) +  a*(f(ix+1,iy,iz) + f(ix-1,iy,iz)) - b*f(ix,iy,iz)
    end do
    end do
    end do
    !$omp end do nowait
  endif

  if (m(2)%n > 1) then 
    a = 1./(m(2)%d)**2 ; b = 2./(m(2)%d)**2
    !$omp do
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      laplace(ix,iy,iz) = &
      laplace(ix,iy,iz) +  a*(f(ix,iy+1,iz) + f(ix,iy-1,iz)) - b*f(ix,iy,iz)
    end do
    end do
    end do
    !$omp end do nowait
  endif

  if (m(3)%n > 1) then 
    a = 1./(m(3)%d)**2 ; b = 2./(m(3)%d)**2
    !$omp do
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      laplace(ix,iy,iz) = &
      laplace(ix,iy,iz) +  a*(f(ix,iy,iz+1) + f(ix,iy,iz-1)) - b*f(ix,iy,iz)
    end do
    end do
    end do
    !$omp end do nowait
  endif
  !$omp end parallel
END FUNCTION laplace

!*******************************************************************************
FUNCTION ddx (a) RESULT (b)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: a, b
  integer:: iz
!...............................................................................
  !$omp parallel do
  do iz=m(3)%lb,m(3)%ub
    b(:,:,iz) = 0.0
  end do
  call ddx_add (a, b, +1.0)
END FUNCTION ddx
!*******************************************************************************

SUBROUTINE ddx_add (a, b, sign)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: a, b
  integer :: idim, ix, iy, iz
  real    :: ax, bx, sign
  !
  ax = 8. / (12.*m(1)%d); bx = -1. / (12.*m(1)%d)
  if (m(1)%n > 1) then ! x-derivative only non-zero iff x-dimension larger than 1
    !$omp parallel do private(ix,iy,iz)
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      b(ix,iy,iz) = b(ix,iy,iz) + sign* &
                   (ax*(a(ix+1,iy,iz) - a(ix-1,iy,iz)) &
                  + bx*(a(ix+2,iy,iz) - a(ix-2,iy,iz)))                         ! d(vx)/dx
    end do
    end do
    end do
  endif
  call scalar_stats (a, 'ddx_add:in')
  call scalar_stats (b, 'ddx_add:out')
END SUBROUTINE ddx_add

!*******************************************************************************
FUNCTION ddy (a) RESULT (b)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: a, b
  integer:: iz
!...............................................................................
  !$omp parallel do
  do iz=m(3)%lb,m(3)%ub
    b(:,:,iz) = 0.0
  end do
  call ddy_add (a, b, +1.0)
END FUNCTION ddy

!*******************************************************************************
SUBROUTINE ddy_add (a, b, sign)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: a, b
  integer :: idim, ix, iy, iz
  real    :: ay, by, sign
  !
  ay = 8. / (12.*m(2)%d); by = -1. / (12.*m(2)%d)
  !
  if (m(2)%n > 1) then
    !$omp parallel do private(ix,iy,iz)
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      b(ix,iy,iz) = b(ix,iy,iz) + sign* &
                   (ay*(a(ix,iy+1,iz) - a(ix,iy-1,iz)) &
                  + by*(a(ix,iy+2,iz) - a(ix,iy-2,iz)))                         ! d(vy)/dy
    end do
    end do
    end do
  endif
  call scalar_stats (a, 'ddy_add:in')
  call scalar_stats (b, 'ddy_add:out')
END SUBROUTINE ddy_add

!*******************************************************************************
FUNCTION ddz (a) RESULT (b)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: a, b
  integer:: iz
!...............................................................................
  !$omp parallel do
  do iz=m(3)%lb,m(3)%ub
    b(:,:,iz) = 0.0
  end do
  call ddz_add (a, b, +1.0)
END FUNCTION ddz

!*******************************************************************************
SUBROUTINE ddz_add (a, b, sign)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: a, b
  integer :: idim, ix, iy, iz
  real    :: az, bz, sign
  !
  az = 8. / (12.*m(3)%d); bz = -1. / (12.*m(3)%d)
  !
  if (m(3)%n > 1) then
    !$omp parallel do private(ix,iy,iz)
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      b(ix,iy,iz) = b(ix,iy,iz) + sign* &
                   (az*(b(ix,iy,iz+1) - b(ix,iy,iz-1)) &
                  + bz*(b(ix,iy,iz+2) - b(ix,iy,iz-2)))                         ! d(vz)/dz
    end do
    end do
    end do
  endif
  call scalar_stats (a, 'ddz_add:in')
  call scalar_stats (b, 'ddz_add:out')
END SUBROUTINE ddz_add

!*******************************************************************************
SUBROUTINE scalar_stats (f, c)
  USE mpi_reduce
  USE io, only: do_debug
  implicit none
  real, dimension(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub):: f
  real:: mn, av, mx
  real, dimension(:,:,:), allocatable:: interior
  character(len=*):: c
!...............................................................................
  if (.not.do_debug) return
  allocate (interior(m(1)%n, m(2)%n, m(3)%n))
  interior = f(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1)                                ! skip ghost zones
  mn = minval(interior);            call min_real_mpi(mn)                       ! global min
  av = sum(interior)/product(m%gn); call sum_real_mpi(av)                       ! global average
  mx = maxval(interior);            call max_real_mpi(mx)                       ! global max
  if (master) print '(1x,a8,3x,a,1p,3e11.3)', c, ' min, aver, max :', mn, av, mx
  deallocate (interior)
END SUBROUTINE scalar_stats

!*******************************************************************************
END MODULE operators
