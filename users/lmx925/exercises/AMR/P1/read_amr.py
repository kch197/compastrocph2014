""" Based on a Mayavi example
"""
import time
import numpy as np
from scipy import special
from numpy import fromfile, float32, prod, transpose
from matplotlib.pyplot import imshow, show, draw

# Read the data (you may want to have a for loop here)
n=64
shape=(n,n)
taskid=0
for i in range(5):
    filename='id={:0=7d}_{:0=5d}.dat'.format(taskid,i+1)
    print(filename)
    fd=open(filename,'rb')
    dd=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    ds=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Ux=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Uy=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Uz=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Bx=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    By=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Bz=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    #Bz=transpose(Bz)
    if (i==0):
        im=imshow(Bz,origin='lower')
        show()
    im.set_data(Bz)
    draw()
    time.sleep(0.4)
