!*******************************************************************************
PROGRAM induction_test
  USE mesh                                                                      ! mesh module
  USE operators                                                                 ! div() operator
  USE boundaries                                                                ! ghost zone handling
  USE mpi_coords                                                                ! MPI dimensions
  USE mpi_reduce                                                                ! max, min, average
  USE io                                                                        ! input/output module
  implicit none
  character(len=mch), save:: id= &
    'main.f90 $Id$'
  integer :: istep, iout                                                        ! step and i/o counters
  real, allocatable, dimension(:,:,:):: bx, dbxdt                               ! magnetic field
  real, allocatable, dimension(:,:,:):: by, dbydt                               ! magnetic field
  real, allocatable, dimension(:,:,:):: bz, dbzdt                               ! magnetic field
  real, allocatable, dimension(:,:,:) :: interior                               ! without ghost zones
  real:: dt                                                                     ! time step
  integer:: nstep=100                                                           ! number of time steps
  integer:: fout                                                                ! output frequency
  integer:: ix, iy, iz                                                          ! loop indices
  real:: u(3) = (/1.0,0.5,0.0/)                                                 ! velocity components
  real:: width=10.0                                                             ! Gaussian width
  real:: courant=0.3                                                            ! Courant number
  logical:: split_mpi=.true.                                                    ! overlap MPI & operator
  namelist /induction/ u, width, courant, nstep, fout, split_mpi                ! input namelist
  real(8):: t                                                                   ! model time
  real(8):: wc                                                                  ! wall clock
!-------------------------------------------------------------------------------
! Initialize mesh
!-------------------------------------------------------------------------------
  call init_mpi                                                                 ! start mpi
  call print_id(id)                                                             ! identify version
  call init_io                                                                  ! setup input/output
  call init_mesh                                                                ! setup mesh
  call init_boundary                                                            ! setup domain boundaries
!-------------------------------------------------------------------------------
! Setup initial conditions
!-------------------------------------------------------------------------------
  rewind (input); read (input,induction); if (master) write(*,induction)        ! read namelist input
  allocate (bx(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub), &              ! allocate x-density
         dbxdt(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub))                ! allocate x-density derivative
  allocate (by(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub), &              ! allocate y-density
         dbydt(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub))                ! allocate y-density derivative
  allocate (bz(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub), &              ! allocate z-density
         dbzdt(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub))                ! allocate z-density derivative
  allocate (b(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub))                 ! allocate density
  bx = 0
  dbxdt = 0
  by = 0
  dbydt = 0
  dbzdt = 0
  do iz=0,m(3)%n-1; do iy=0,m(2)%n-1; do ix=0,m(1)%n-1                          ! loop over 3-D
    bz(ix,iy,iz) = exp(-(m(1)%r(ix)**2+m(2)%r(iy)**2)/width**2)                 ! Gaussian, centered in (x,y)
  end do; end do; end do
  call boundary(bx)                                                             ! fill ghost zones
  call boundary(by)                                                             ! fill ghost zones
  call boundary(bz)                                                             ! fill ghost zones
!-------------------------------------------------------------------------------
! Setup output
!-------------------------------------------------------------------------------
  allocate(interior(m(1)%n,m(2)%n,m(3)%n))                                      ! buffer for I/O
  iout = 0                                                                      ! zero based snapshot
  call file_openw_mpi ('snapshot.dat', m%gn, m%n, m%offset)                     ! open data file
  t = 0d0
  call write_field(bx)                                                          ! dump data
  call write_field(by)                                                          ! dump data
  call write_field(bz)                                                          ! dump data
!-------------------------------------------------------------------------------
! Evolve the solution in time
!-------------------------------------------------------------------------------
  dt = courant*minval(m%d)/sqrt(sum(u**2))                                      ! dt = C dx/u
  wc = wallclock()                                                              ! start timer
  do istep = 1, nstep                                                           ! time step loop
    bx = bx - dt*(ddy(u(2)*bx)+ddz(u(3)*bx))                                    ! Bx induction equation
    by = by - dt*(ddz(u(3)*by)+ddx(u(1)*by))                                    ! By induction equation
    bz = bz - dt*(ddx(u(1)*bz)+ddy(u(2)*bz))                                    ! Bz induction equation
    call boundary(bx)                                                           ! fill ghost zones
    call boundary(by)                                                           ! fill ghost zones
    call boundary(bz)                                                           ! fill ghost zones
    t = t + dt                                                                  ! increment model time
    if (modulo(istep,fout)==0) then
        call write_field(bx)
        call write_field(by)                                                          ! dump data
        call write_field(bz)                                                          ! dump data
    end if
  enddo
  wc = wallclock()-wc                                                           ! read off timer
  if (master) print'(a,f6.1,a,f6.1)', &                                         ! to get accurate results ...
    'time:',wc,' s,  ns/p:',wc*1e9/(nstep*product(real(m%n)))                   ! ... use nout > nstep
!-------------------------------------------------------------------------------
! Clean up
!-------------------------------------------------------------------------------
  call file_close_mpi                                                           ! close snapshot file
  deallocate (bx, dbxdt)                                                        ! dellocate density
  deallocate (by, dbydt)                                                        ! dellocate density
  deallocate (bz, dbzdt)                                                        ! dellocate density
  call end_mpi                                                                  ! end mpi
CONTAINS

!*******************************************************************************
SUBROUTINE write_field(f)
  implicit none
  real f(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub)
  real:: mn, av, mx
!...............................................................................
  interior = f(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1)                                ! skip ghost zones
  call file_write_mpi (interior, m%n, iout)                                     ! collective write
  mn = minval(interior); call min_real_mpi(mn)                                  ! global min
  av = sum(interior)/product(m%gn); call sum_real_mpi(av)                       ! global average
  mx = maxval(interior); call max_real_mpi(mx)                                  ! global max
  if (master) print '(i5,3x,a,1p,e11.4,a,3e12.4)', &                            ! write one-liner
    iout, 't =', t, 'snap, min, aver, max :', mn, av, mx                        ! printout
  iout = iout + 1                                                               ! increment snapshot
END SUBROUTINE write_field
END PROGRAM
