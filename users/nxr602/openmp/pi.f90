!*******************************************************************************
!
! Example OpenMP program.  Try compiling this with OpenMP option:
!
!   ifort -openmp openmp1.f90 -o openmp1.x
!   gfortran -ofpenmp openmp1.f90 -o openmp1.x
!
! Then run it with one core, and with -- for openmp -- 4 cores::
!
!   export OMP_NUM_THREADS=1; ./openmp1.x      # with bash shell
!   export OMP_NUM_THREADS=4; ./openmp1.x      # with bash shell
!
!   setenv OMP_NUM_THREADS 1; ./openmp1.x      # with tcsh shell
!   setenv OMP_NUM_THREADS 4; ./openmp1.x      # with tcsh shell
!
!
!*******************************************************************************
PROGRAM pi
  implicit none
  integer:: n, i
  double precision :: s, sp

  !$omp parallel default(none) &
  !$omp private(i,sp) shared(s,n)

  !$omp single
  s = 0.0D0
  n = 1000000000
  !$omp end single

  sp = 0.0D0

  !$omp do
  do i=0,n
     sp = sp + 4.0D0*(-1)**(i)/(2.0D0*i+1.0D0)
  enddo
  !$omp enddo
  !$omp critical
    s = s + sp
  !$omp end critical
  !$omp end parallel

  print *,'pi =',s

END PROGRAM pi
