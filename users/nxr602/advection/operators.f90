MODULE operators
  USE mesh,       only: m
  USE boundaries, only: boundary
  implicit none
CONTAINS
!*******************************************************************************
! Divergence and curl operator -- uses 4th order centered finite difference
!*******************************************************************************
FUNCTION div(vx,vy,vz)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: div
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: vx,vy,vz
  !
  div = ddx(vx) + ddy(vy) + ddz(vz)
  call boundary (div)
END FUNCTION div
!*******************************************************************************
SUBROUTINE curl(ax,ay,az,bx,by,bz)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: ax,ay,az
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: bx,by,bz
  !
  bx = ddy(az) - ddz(ay)
  by = ddz(ax) - ddx(az)
  bz = ddx(ay) - ddy(ax)
  call boundary(bx)
  call boundary(by)
  call boundary(bz)
END SUBROUTINE curl
!*******************************************************************************
FUNCTION ddx(vx)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: ddx
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: vx
  integer :: idim, ix, iy, iz
  real    :: ax, bx
  !
  ax = 8. / (12.*m(1)%d); bx = -1. / (12.*m(1)%d)
  if (m(1)%n > 1) then ! x-derivative only non-zero iff x-dimension larger than 1
    !$omp parallel default(none) &
    !$omp   private(ix,iy,iz) shared(div,ax,ay,az,bx,by,bz,vx,vy,vz)
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    !$omp do
    do ix=0,m(1)%n-1
      ddx(ix,iy,iz) = ax*(vx(ix+1,iy,iz) - vx(ix-1,iy,iz)) &
                    + bx*(vx(ix+2,iy,iz) - vx(ix-2,iy,iz))                     ! d(vx)/dx
    end do
    !$omp enddo
    end do
    end do
    !$omp end parallel
  else
    ddx = 0.0
  endif
END FUNCTION ddx
!*******************************************************************************
FUNCTION ddy(vy)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: ddy
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: vy
  integer :: idim, ix, iy, iz
  real    :: ay, by
  !
  ay = 8. / (12.*m(2)%d); by = -1. / (12.*m(2)%d)
  !
  if (m(2)%n > 1) then
    !$omp parallel default(none) &
    !$omp   private(ix,iy,iz) shared(div,ax,ay,az,bx,by,bz,vx,vy,vz)
    do iz=0,m(3)%n-1
    !$omp do
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      ddy(ix,iy,iz) = ay*(vy(ix,iy+1,iz) - vy(ix,iy-1,iz)) &
                    + by*(vy(ix,iy+2,iz) - vy(ix,iy-2,iz))                     ! d(vy)/dy
    end do
    end do
    !$omp enddo
    end do
    !$omp end parallel
  else
    ddy = 0.0
  endif
END FUNCTION ddy
!*******************************************************************************
FUNCTION ddz(vz)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: ddz
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: vz
  integer :: idim, ix, iy, iz
  real    :: az, bz
  !
  az = 8. / (12.*m(3)%d); bz = -1. / (12.*m(3)%d)
  !
  if (m(3)%n > 1) then
    !$omp parallel default(none) &
    !$omp   private(ix,iy,iz) shared(div,ax,ay,az,bx,by,bz,vx,vy,vz)
    !$omp do
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      ddz(ix,iy,iz) = az*(vz(ix,iy,iz+1) - vz(ix,iy,iz-1)) &
                    + bz*(vz(ix,iy,iz+2) - vz(ix,iy,iz-2))                     ! d(vz)/dz
    end do
    end do
    end do
    !$omp enddo
    !$omp end parallel
  else
    ddz = 0.0
  endif
END FUNCTION ddz
END MODULE operators
