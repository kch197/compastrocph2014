""" read and show advection data """
from numpy import reshape, fromfile, float32, prod
import matplotlib.pyplot as plt

shape = (64,64)
fd = open('snapshot_1.dat', 'rb')

plt.ion()
i = 1
while True:
  print i
  data = fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
  plt.imshow(data,origin='lower')
  plt.draw()
  i += 1
plt.ioff()
