!*******************************************************************************
! Data type that describes a patch, which is the units we compute on.  This
! data type is the only one shared (synced) btw all ranks, at leat initially.
!*******************************************************************************
MODULE patch_mod
  USE task_mod, only: task_t, task_table, ntask, allocate_task
  USE mesh_mod, only: box, read_mesh
  USE io
  implicit none
  integer, parameter:: ntime=3
PRIVATE
!-------------------------------------------------------------------------------
  type:: patch_t
    integer:: id = 1                    ! unique id
    integer:: upload = -1               ! ID to upload to (only in tests)
    integer:: rank = 0                  ! MPI rank
    integer:: win = -1                  ! MPI window
    integer:: father = 1                ! father id (no longer relevant)
    integer:: level = 1                 ! refinement level
    real(kind=4):: size(3) = 1.0        ! real(4) is exact to more than 100 levels!
    real(kind=16):: pos(3,ntime) = 0.5  ! real(16), since can be any position 
    logical:: refined = .false.         ! false only for leaf cells
    real   :: load = 1.0                ! relative cost of advancing this patch
    integer:: ncell = 8                 ! patch cell size (suggestion)
    integer:: nborder = 2               ! number of ghost zones
    type(task_t), pointer:: task        ! point to the task handling this patch
  end type
  integer:: npatch=0, maxpatch
  type(patch_t), pointer:: patch_table(:)
!-------------------------------------------------------------------------------
PUBLIC:: patch_t, patch_table, npatch, maxpatch, read_patches, sync_patches
CONTAINS

!*******************************************************************************
! Read (or otherwise define) patches and put them into the patch_table() array
! on all ranks.   Other similar routines should do the same.
! FIXME: This routine and data needed for it should be moved to patch_mod.f90.
!*******************************************************************************
SUBROUTINE read_patches
  implicit none
  integer:: ip, it, level, iostat
  type(task_t), pointer:: task
!...............................................................................
  rewind(input)
  do ip=npatch+1,maxpatch
    patch_table(ip)%task => task_table(ip)
    task => task_table(ip)
    !
    call read_mesh (task%m, level, iostat)
    do it=1,task%nt
      task%pos(:,it) = task%m%p
      patch_table(ip)%pos(:,it) = task%m%p
    end do
    task%level = level
    task%quality = level
    !
    task%m%id = task%id 
    if (iostat /= 0) exit
    task%id = ip
    task%m%id = ip
    task%it = 1
    ! Set box size = size of first patch
    if (ip==1) box = task%m%s
    patch_table(ip)%size = task%m%s + 2*task%m%ng*task%m%d
    if (do_trace) print*,'read patch nr',ip,' dx =',&
      task%m(1)%d, task%m(1)%lb, task%m(1)%ub
  end do
  ! Save number of patches
  npatch = ip-1                                                                  
  if (master) print*,'npatch, maxpatch, box =', npatch, maxpatch, box
END SUBROUTINE read_patches

!*******************************************************************************
! FIXME: This routine should send the patches we "own" to all other ranks, and
! should receive from all other ranks the patches they own.  This is a typical
! task for MPI_Alltoallv, so we need to 
!   1. build a contiguous send buffer and record its size
!   2. use alltoall_mpi to exchange info about these sizes
!   3. use that information to setup a call to alltoallv_mpi
!   4. unpack the receive buffer into place
! As part of this operation we might as well sort the patches into quality 
! order, which is what we need in the download operations.  
!*******************************************************************************
SUBROUTINE sync_patches
  implicit none
END SUBROUTINE sync_patches

!*******************************************************************************
END MODULE patch_mod
