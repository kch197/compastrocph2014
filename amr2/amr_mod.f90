!*******************************************************************************
! Module with skeleton data type for AMR.  At least for now, we will keep things 
! simple, by maintaining an MPI-synchronized structure with a complete set of all
! patch positions and relations in space.  This is the task of this module, which 
! defines the derived data type and the related methods.  The data type is low cost, 
! with only about 100 bytes per patch, so even a million patches is a small amount 
! of the memory of one node.  A practical limit is perhaps to have at most 1e7 
! patches, before this starts to become a limitation. Each patch can hold for 
! example 32^3 cells for up to 3 billion cells.  With 10 mus/update and a desire 
! to do one update per second on all patches (conservative estimate since it 
! disregards the inherent sub-cycling), this starts to become a limitation when 
! using more than 3e9*1e-5 = 1e4 nodes (not cores!).  So, we are fine up to several 
! hundred thousand cores, for now.
!*******************************************************************************
MODULE amr_mod
  USE patch_mod, only: patch_t, patch_table, npatch, maxpatch
  USE task_mod,  only: task_t, task_table, ntask
  USE mesh_mod,  only: mesh_t, read_mesh, box
  USE io
  implicit none
!-------------------------------------------------------------------------------
  type:: amr_t
    integer:: levelmin, levelmax      ! min and max refinement
  end type
!-------------------------------------------------------------------------------
  type(amr_t), target:: amr
  type(patch_t), private:: patch
  integer, private:: patch_bytes
CONTAINS

!*******************************************************************************
SUBROUTINE init_amr
  implicit none
  real:: amrgb=0.1
  integer:: levelmin=5, levelmax=5, maxamr=0, ip
  namelist /amr_params/ levelmin, levelmax, maxamr, amrgb, do_trace, do_debug
  character(len=mch), save:: id= &
  'amr_mod.f90 $Id$'
!...............................................................................
  call print_id (id)
  call trace_begin ('init_amr')
  rewind(input); read(input,amr_params); if(master) write(*,amr_params)
  do_trace = do_trace.and.master
  do_debug = do_debug.and.master
  patch_bytes=sizeof(patch)
  if (maxamr==0) maxamr = amrgb*1e9/patch_bytes
  maxpatch = maxamr
  if (master) print &
  '(1x,"allocating",f7.3," GB per rank to AMR table, for max",f8.3," million patches")', &
    maxamr*patch_bytes*1e-9, real(maxamr)*1e-6
  allocate (patch_table(maxpatch))
  do ip=1,maxpatch
    patch_table(ip)%id = ip
  end do
  npatch = 0
  npatch = 0
  call trace_end
END SUBROUTINE init_amr

!*******************************************************************************
SUBROUTINE deallocate_amr (amr)
  implicit none
  type(amr_t):: amr
!...............................................................................
  deallocate (patch_table)
 !amr%is_allocated = .false.
END SUBROUTINE deallocate_amr

!*******************************************************************************
SUBROUTINE print_amr (a)
  implicit none
  integer i, nf
  type(amr_t):: a
!...............................................................................
  print'(a)', '  id   rank father level     size           position       '// &
  '              sons                neighbor     foregn ref  load'
  nf = 0
  do i=1,npatch
    print'(i4,i7,i7,i6,f9.6,3x,3f8.5,2x,8i3,2x,6i3,i6,l4,f6.0)',  &
      patch_table(i)%id, patch_table(i)%rank, patch_table(i)%level !, &
      !patch_table(i)%size, patch_table(i)%pos, patch_table(i)%son, patch_table(i)%neighbor, &
      !patch_table(i)%foreign, patch_table(i)%refine, patch_table(i)%load
  end do
  print *,'total foreigners:', nf
END SUBROUTINE print_amr

!*******************************************************************************
END MODULE amr_mod
