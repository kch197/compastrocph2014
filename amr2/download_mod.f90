!*******************************************************************************
MODULE download_mod
  USE mesh_mod,     only: mesh_t, box
  USE task_mod,     only: task_t, allocate_task, ntask, task_table
  USE patch_mod,    only: patch_t, patch_table, npatch
  USE mpi_rma,      only: win_lock_x_mpi, get_reals_mpi, win_unlock_mpi
  USE interpol_mod, only: interpolate
  USE io_amr,       only: write_amr
  USE io
  implicit none
PRIVATE
!...............................................................................
PUBLIC:: download
CONTAINS

!*******************************************************************************
! Check if there is sufficient overlap from patch p2, so we can download values
! from it. We need to be able to download at least some boundary values based on 
! values relevant for our current time.  We should decide if a single time slice 
! on that patch is enough, or (better) if two are needed, or more.  Here we ask,
! for simplicity, if ALL time slices overlap.
!*******************************************************************************
LOGICAL FUNCTION overlap (p1, p2)
  type(patch_t):: p1, p2
  type(task_t), pointer:: t1
  real(8):: lim(3)
  real(8):: dist
  integer:: it, dir
!...............................................................................
  t1 => p1%task
  lim = 0.5d0*(p1%size+p2%size)
  overlap = .false.
  if (p1%task%id==p2%task%id.and.p1%task%id>1) return
  overlap = .true.
  do it=1,p2%task%nt
    overlap = overlap .and. &
      all(abs(modulo(p1%task%pos(:,t1%it)-p2%task%pos(:,it)+0.5d0*box,box)-0.5d0*box)<=lim)
    if (do_debug.and.master) then
      do dir=1,3
        print'("overlap:",2i4,2i3,2f10.6,l5)', &
         p1%id,p2%id,t1%it,it,p1%task%pos(dir,it),p2%task%pos(dir,it),overlap
      end do
    end if
    !do dir=1,3
      !dist = p1%pos(dir,t1%it)-p2%pos(dir,it)+0.5d0*box(dir)
      !dist = modulo(dist,box(dir))
      !dist = dist - 0.5
      !overlap = overlap .and. abs(dist)<=lim(dir)
      !if (do_trace) print'("overlap:",2i4,2i3,2f10.6,l5)', &
      !  p1%id,p2%id,t1%it,it,p1%pos(dir,it),p2%pos(dir,it),overlap
    !end do
  end do
END FUNCTION overlap

!*******************************************************************************
! If the patch is owned by a different rank, get a (passive) copy to the current
! rank.
!*******************************************************************************
SUBROUTINE get_patch (p2)
  implicit none
  type(patch_t), pointer:: p2
  type(task_t), pointer:: t2
!...............................................................................
  if (p2%task%rank == mpi_rank) return                          ! already OK!
  npatch = npatch+1                                               ! take a new slot
  patch_table(npatch) = p2                                         ! copy the p2 data
  p2 => patch_table(npatch)                                        ! point to new slot
  t2 => p2%task                                                 ! short hand
  call win_lock_x_mpi (p2%win, p2%rank)                         ! lock the window
  call get_reals_mpi (p2%win, t2%mem, t2%nmem, 0, p2%rank)      ! get the data
  call win_unlock_mpi (p2%win, p2%rank)                         ! unlock the window
  t2%rank = mpi_rank                                            ! now local
  t2%passive = .true.                                           ! mark as passive
END SUBROUTINE get_patch

!*******************************************************************************
! Handle downloading of values from all of the tasks that overlap and have relevant 
! data. This should be done in some ordered fashion, so the best quality data is 
! loaded on top of lower quality data. The simplest way to do this is to sort the 
! patches in order of increasing quality -- here we assume that this has been 
! done. Any patch that has higher quality data than "our" patch is also allowed 
! to overload data inside our domain proper.
!*******************************************************************************
SUBROUTINE download (id)
  implicit none
  integer:: id
  type(patch_t), pointer:: p1, p2
  type(task_t), pointer:: t1, t2
  type(mesh_t):: m(3)
  integer:: i, ix, iy, iz, n(3), count, it, jt, kt, lt(2)
  real(8):: time1, time2(3)
  real::  pt(2)
  logical:: bdry_only
!...............................................................................
  p1 => patch_table(id)
  do i=1,npatch
    p2 => patch_table(i)
    if (overlap(p1,p2)) then
      call get_patch(p2)
      t1 => p1%task
      t2 => p2%task
      if (all(t2%t==0d0)) then                    ! don't download from unevolved patch
        if (do_debug) print &
          '("task ",i4," time slot",i2," cannot use unevolved task",i7)', &
          p1%id, p1%task%it, p2%id
         cycle
      end if
      time1 = t1%t(t1%it)                         ! time on our patch
      kt = 1                                      ! time2(kt) = largest time < time1
      time2 = -99.                                ! make sure out of scope
      do it=1,t2%nt                               ! time2 in increasing index order
        jt = modulo(t2%it+it-1,t2%nt)+1           ! it=1 => index "after" t2%it
        time2(it) = t2%t(jt)                      ! 1-indexed time
        if (time2(it)<=time1 .and. it<t2%nt) kt=it ! first index in bracket
      end do
      lt(1) = modulo(t2%it+kt-1,t2%nt)+1          ! t2 time index of 1st time
      lt(2) = modulo(t2%it+kt  ,t2%nt)+1          ! t2 time index of 2nd time
      !-------------------------------------------------------------------------
      ! Weights for time interpolation / extrapolation
      !-------------------------------------------------------------------------
      pt(2) = (time1-time2(kt))/max(tiny(1d0),time2(kt+1)-time2(kt))
      pt(1) = 1.0-pt(2)
      if (abs(pt(1)-0.5d0)> 1.0) cycle            ! don't extrapolate more than 50%
      t1%pt = pt(2)                               ! save inter/extrap weight for info
      !--------------------------------------------------------------------------
      ! If the other patch does not have better quality data, use its data only
      ! in the ghost zones -- otherwise use all of it.  This covers also the case
      ! where the two patches are the same (root grid), and have periodic overlap.
      !--------------------------------------------------------------------------
      bdry_only = p2%task%quality <= p1%task%quality
      n = p1%task%n
      m = p1%task%m
      !-------------------------------------------------------------------------
      ! By looping backwards over indices we can benefit from the periodic 
      ! wrapping of the largest (root) grid.  Normally, the upper limit for the
      ! floating point index (m%uf) is set to (m%ui-1), but for the root grid 
      ! it is set to m%ui, so interpolation is done also in the last interval.
      ! This is OK, provided that the periodic point has already been computed,
      ! which it has when we loop backwards.
      !-------------------------------------------------------------------------
      count = 0
      do iz=m(3)%ub,m(3)%lb,-1
        do iy=m(2)%ub,m(2)%lb,-1
          do ix=m(1)%ub,m(1)%lb,-1
            if (bdry_only.and.(ix>=m(1)%li).and.(ix<=m(1)%ui) &
                         .and.(iy>=m(2)%li).and.(iy<=m(2)%ui) &
                         .and.(iz>=m(3)%li).and.(iz<=m(3)%ui)) cycle
            call interpolate (p1%task, p2%task, pt, lt, ix, iy, iz, count)
          end do
        end do
      end do
      if (count>0.and.do_trace.or.do_debug) print &
        '("task ",i4," time slot",i2," downloaded",i6," cells from task",i7," time slots",2i3,2f7.2)', &
        p1%id, p1%task%it, count, p2%id, lt, pt
    else
      if (do_debug) print &
        '("task ",i4," time slot",i2," has no overlap with task",i7," time slots",2i3,2f7.2)', &
        p1%id, p1%task%it, p2%id, lt, pt
    end if
  end do
END SUBROUTINE download

!*******************************************************************************
END MODULE download_mod
