MODULE velocity_fields
  USE io
  implicit none
CONTAINS

!*******************************************************************************
SUBROUTINE init_velocity (UU, u, u0, type, m, pos)
  USE mpi_base,   only: master
  USE mesh_mod,   only: mesh_t
  USE vector_mod, only: vector_t, allocate_vector, deallocate_vector
  USE scalar_mod, only: allocate_scalar, deallocate_scalar
  USE io,         only: input
  implicit none
  type(vector_t):: UU
  type(vector_t):: k
  type(mesh_t):: m(3)
  real:: u(3), u0(3), pos(3)
  character(len=16):: type
  integer ix,iy,iz
  real:: px, py, pz
  real:: pi2 = 8.0*atan(1.0)
!-------------------------------------------------------------------------------
! Select type => coefficients in expressions
!-------------------------------------------------------------------------------
  call trace_begin ('init_velocity')
!-------------------------------------------------------------------------------
! (x,y,z)-cordinates normalized to (-pi,pi)
!-------------------------------------------------------------------------------
  call trace ('allocate')
  call allocate_vector (m, k)
  do iz=m(3)%lb,m(3)%ub
  do iy=m(2)%lb,m(2)%ub
  do ix=m(1)%lb,m(1)%ub
    k%x(ix,iy,iz) = pi2*(m(1)%r(ix)+m(1)%p-pos(1))/m(1)%s
    k%y(ix,iy,iz) = pi2*(m(2)%r(iy)+m(2)%p-pos(2))/m(2)%s
    k%z(ix,iy,iz) = pi2*(m(3)%r(iz)+m(3)%p-pos(3))/m(3)%s
  end do
  end do
  end do
!-------------------------------------------------------------------------------
! Model expressions for velocity field UU
!-------------------------------------------------------------------------------
  call trace ('select')
  select case (trim(type))
  case('advect')
    UU%x =  0.0                         ! constant advection speed
    UU%y =  0.0                         ! constant advection speed
    UU%z =  0.0                         ! constant speed along B
  case('converge')
    UU%x = -u(1)*sin(k%x)               ! convergence towards x=y=0
    UU%y = -u(2)*sin(k%y)               ! convergence towards x=y=0
    UU%z =  u(3)                        ! constant speed along B
  case('diverge')
    UU%x =  u(1)*sin(k%x)               ! divergence away from x=y=0
    UU%y =  u(2)*sin(k%y)               ! divergence away from x=y=0
    UU%z =  u(3)                        ! constant speed along B
  case('bend')
    UU%x =  u(1)*sin(k%z)               ! shear in the z-direction
    UU%y =  u(2)*sin(k%z)               ! shear in the z-direction
    UU%z =  u(3)                        ! constant speed along B
  case('twist')
    UU%x = +u(1)*sin(k%y)*sin(k%z)      ! rotation increasing with z
    UU%y = -u(2)*sin(k%x)*sin(k%z)      ! rotation increasing with z
    UU%z =  u(3)                        ! constant speed along B
  case default
    print*,'unknown velocity type',type
  end select
  UU%x = UU%x + u0(1)
  UU%x = UU%x + u0(2)
  UU%x = UU%x + u0(3)
  call deallocate_vector(k)
  call trace_end
END SUBROUTINE init_velocity

END MODULE velocity_fields
