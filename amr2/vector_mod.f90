!*******************************************************************************
! Vector routines
!*******************************************************************************
MODULE vector_mod
  USE mesh_mod,   only: mesh_t
 !USE boundaries, only: boundary
  USE scalar_mod, only: ddx, ddy, ddz, ddx_add, ddy_add, ddz_add
  USE io
PUBLIC
  type :: vector_t
    real, dimension(:,:,:,:), pointer:: mem
    real, dimension(:,:,:), pointer:: x, y, z
    logical:: is_allocated = .false.
  end type
CONTAINS

!*******************************************************************************
SUBROUTINE init_vector (mem, v)
  real, dimension(:,:,:,:), pointer:: mem
  type(vector_t):: v
!...............................................................................
  v%x => mem(:,:,:,1)
  v%y => mem(:,:,:,2)
  v%z => mem(:,:,:,3)
  v%mem => mem
END SUBROUTINE init_vector

!*******************************************************************************
SUBROUTINE allocate_vector (m, v)
  implicit none
  type(mesh_t):: m(3)
  type(vector_t):: v
  real, dimension(:,:,:,:), pointer:: mem
!...............................................................................
  if (v%is_allocated) return
  call trace_begin('allocate_vector')
  allocate (v%mem(m(1)%gn, m(2)%gn, m(3)%gn, 3))
  call init_vector (v%mem, v)
  v%is_allocated = .true.
  call trace_end
END SUBROUTINE allocate_vector

!*******************************************************************************
SUBROUTINE deallocate_vector (v)
  implicit none
  type(vector_t):: v
!...............................................................................
  if (.not. v%is_allocated) return
  deallocate (v%mem)
  v%is_allocated = .false.
END SUBROUTINE deallocate_vector

!*******************************************************************************
SUBROUTINE boundary_vector (a)
  implicit none
  type(vector_t):: a
!...............................................................................
  !call boundary(a%x)
  !call boundary(a%y)
  !call boundary(a%z)
END SUBROUTINE boundary_vector

!*******************************************************************************
SUBROUTINE cross_subr (a, b, c)
  implicit none
  type(vector_t):: a, b, c
!...............................................................................
  c%x = a%y*b%z - a%z*b%y
  c%y = a%z*b%x - a%x*b%z
  c%z = a%x*b%y - a%y*b%x
END SUBROUTINE cross_subr
!*******************************************************************************
FUNCTION dot (m, a, b) RESULT(c)
  implicit none
  type(mesh_t):: m(3)
  type(vector_t):: a, b
  real, dimension(m(1)%gn,m(2)%gn,m(3)%gn):: c
  integer, dimension(3):: l, u
!...............................................................................
  l = m%li
  u = m%ui
  c = 0.0
    c(l(1):u(1),l(2):u(2),l(3):u(3)) = &
  a%x(l(1):u(1),l(2):u(2),l(3):u(3))*b%x(l(1):u(1),l(2):u(2),l(3):u(3)) + &
  a%y(l(1):u(1),l(2):u(2),l(3):u(3))*b%y(l(1):u(1),l(2):u(2),l(3):u(3)) + &
  a%z(l(1):u(1),l(2):u(2),l(3):u(3))*b%z(l(1):u(1),l(2):u(2),l(3):u(3))
END FUNCTION dot

!*******************************************************************************
SUBROUTINE curl_minus (m, a, b)
  implicit none
  type(mesh_t):: m(3)
  type(vector_t):: a, b
!...............................................................................
  call ddy_add (m, a%z, b%x, -1.0); call ddz_add (m, a%y, b%x, +1.0)
  call ddz_add (m, a%x, b%y, -1.0); call ddx_add (m, a%z, b%y, +1.0)
  call ddx_add (m, a%y, b%z, -1.0); call ddy_add (m, a%x, b%z, +1.0)
  !call boundary_vector(b)
END SUBROUTINE curl_minus

!*******************************************************************************
SUBROUTINE div_minus (m, a, b)
  implicit none
  type(mesh_t):: m(3)
  type(vector_t):: a
  real, dimension(:,:,:), pointer:: b
!...............................................................................
  call ddx_add (m, a%x, b, -1.0)
  call ddy_add (m, a%y, b, -1.0)
  call ddz_add (m, a%z, b, -1.0)
  !call boundary(b)
END SUBROUTINE div_minus

!*******************************************************************************
END MODULE vector_mod
