MODULE refine_mod
  USE patch_mod, only: npatch, patch_table
  USE io
CONTAINS

!*******************************************************************************
! Run through and create sons if the refine flag is set
!*******************************************************************************
SUBROUTINE refine
  implicit none
  integer id, ns
!...............................................................................
  call trace_begin('refine')
  id = 0
  do while (id < npatch)
    id = id+1
    if (patch_table(id)%task%refine) then
     !FIXME: insert code here to add a new patch, with twice the resolution
      patch_table(id)%task%refine = .false.
    end if
  end do
  call trace_end
END SUBROUTINE refine
!*******************************************************************************
END MODULE refine_mod
