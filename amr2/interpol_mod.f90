!*******************************************************************************
MODULE interpol_mod
  USE mesh_mod,   only: mesh_t, box
  USE task_mod,   only: task_t
  USE io
  implicit none
PRIVATE

PUBLIC:: interpolate
CONTAINS

!*******************************************************************************
! The coordinate position are arranged like this, for N=8:
!
!      -S/2              0             +S/2
!  +...+.|.+...+...+...+.|.+...+...+...+.|.+...+
! -2  -1 | 0   1   2   3 | 4   5   6   7 | 8   9
!          0              N/2              N        s_i = (i-(N-1)/2)*(S/N)
!
!*******************************************************************************
SUBROUTINE interpolate (t1, t2, pt, lt, ix, iy, iz, count)
  implicit none
  type(task_t), pointer:: t1, t2
  integer:: ix, iy, iz, count, i, it, jx, jy, jz, jt, kt, iv, iw, lt(2)
  real:: px, py, pz, qx, qy, qz, pt(2), tmp(t2%nv,2), t2pos
  integer, parameter:: verbose=1
!...............................................................................
  !-----------------------------------------------------------------------------
  ! Interpolation in time and space; abandon if just one of the cases is outside
  ! interior of task t2.  We have defined t2%m%o such that (pos-t2%m%o)/t2%m%d
  ! gives the floating point index.
  !-----------------------------------------------------------------------------
  tmp = 0.0
  it = t1%it
  do i=1,2
    jt = lt(i)
    if (pt(i)==0.0) cycle     ! no need to add zero
    t2pos = t2%pos(1,jt) + (t1%t(it)-t2%t(jt))*t2%m(1)%u
    px = t2%m(1)%li + &
      modulo((t1%pos(1,it)-t2pos)+(t1%m(1)%r(ix)-t2%m(1)%o),box(1))/t2%m(1)%d
    if (px<t2%m(1)%lf .or. px>t2%m(1)%uf) then
      !print'(a,i4,5f10.3)','ix,r,o,d,px:',ix,t1%m(1)%r(ix),t2%m(1)%o,t2%m(1)%d,px,t2%m(1)%uf
      return
    end if
    t2pos = t2%pos(2,jt) + (t1%t(it)-t2%t(jt))*t2%m(2)%u
    py = t2%m(2)%li + &
      modulo((t1%pos(2,it)-t2pos)+(t1%m(2)%r(iy)-t2%m(2)%o),box(2))/t2%m(2)%d
    if (py<t2%m(2)%lf .or. py>t2%m(2)%uf) then
      !print'(a,i4,5f10.3)','iy,r,o,d,py:',iy,t1%m(2)%r(iy),t2%m(2)%o,t2%m(2)%d,py,t2%m(2)%uf
      return
    end if
    t2pos = t2%pos(3,jt) + (t1%t(it)-t2%t(jt))*t2%m(3)%u
    pz = t2%m(3)%li + &
      modulo((t1%pos(3,it)-t2pos)+(t1%m(3)%r(iz)-t2%m(3)%o),box(3))/t2%m(3)%d
    if (pz<t2%m(3)%lf .or. pz>t2%m(3)%uf) then
      !print'(a,i4,5f10.3)','iz,r,o,d,pz:',iz,t1%m(3)%r(iz),t2%m(2)%o,t2%m(3)%d,py,t2%m(3)%uf
      return
    end if
    jx = px; px = px-jx; qx = 1.0-px
    jy = py; py = py-jy; qy = 1.0-py
    jz = pz; pz = pz-jz; qz = 1.0-pz
    do iw=1,2
    do iv=1,t2%nv
      tmp(iv,iw) = tmp(iv,iw) + pt(i)*( &
       qz*(qy*(qx*t2%mem(jx,jy  ,jz  ,iv,jt,iw) + px*t2%mem(jx+1,jy  ,jz  ,iv,jt,iw))  &
          +py*(qx*t2%mem(jx,jy+1,jz  ,iv,jt,iw) + px*t2%mem(jx+1,jy+1,jz  ,iv,jt,iw))) &
      +pz*(qy*(qx*t2%mem(jx,jy  ,jz+1,iv,jt,iw) + px*t2%mem(jx+1,jy  ,jz+1,iv,jt,iw))  &
          +py*(qx*t2%mem(jx,jy+1,jz+1,iv,jt,iw) + px*t2%mem(jx+1,jy+1,jz+1,iv,jt,iw))))
    end do
    end do
    if (do_debug) then
      if (verbose>1.or.(verbose>0.and.iy==t1%m(2)%gn/2.and.iz==t1%m(3)%gn/2)) &
        print'(a,2i6,2x,4i4,2x,4i4,3(2x,3f10.3))', &
       'pt',t1%id,t2%id,it,ix,iy,iz,jx,jy,jz,jt,px,py,pz,pt(i),t1%t(t1%it), &
       t2%t(jt),tmp(8,1)
    end if
    count = count+1
  end do
  !-----------------------------------------------------------------------------
  ! Galilean transformation -- compensate for patch speed difference; add the
  ! patch speed of task 2 and subtract out task speed.
  !-----------------------------------------------------------------------------
  if (any(t2%m%u /= t1%m%u)) then
    tmp(3:5,1) = tmp(3:5,1) + (t2%m%u-t1%m%u)*tmp(1,1)
  end if
  !-----------------------------------------------------------------------------
  ! Interpolation in time and space was successful; copy results over
  !-----------------------------------------------------------------------------
  do iw=1,2
  do iv=1,t2%nv
    t1%mem(ix,iy,iz,iv,t1%it,iw) = tmp(iv,iw)
  end do
  end do
  !
END SUBROUTINE interpolate

!*******************************************************************************
END MODULE interpol_mod
