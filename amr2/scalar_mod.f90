!*******************************************************************************
! Scalar data type, consisting of a pointer to a 3-D data cube, and a mesh 
! data type with information about index boundaries.  Two opposing arguments:
! 1) Let scalars be a data type, and refer to scalar%mem for the actual values.
!    In this case operators can get mesh information from the structure, but
!    expressions in pde_mod.f90 become slightly more awkward.
! 2) let scalars be just a pointer to an array.  In this case we need to pass
!    a mesh argument to all scalar operation routines
!*******************************************************************************
MODULE scalar_mod
  USE mesh_mod,   only: mesh_t
 !USE boundaries, only: boundary
  USE io
  implicit none
PUBLIC
CONTAINS

!*******************************************************************************
SUBROUTINE init_scalar (mem, s)
  real, dimension(:,:,:), pointer:: mem, s
!...............................................................................
  s => mem
END SUBROUTINE init_scalar

!*******************************************************************************
SUBROUTINE allocate_scalar (m, s)
  implicit none
  type(mesh_t):: m(3)
  real, dimension(:,:,:), pointer:: mem, s
!...............................................................................
  call trace_begin('allocate_scalar')
  allocate (mem(m(1)%gn, m(2)%gn, m(3)%gn))
  s => mem
  call trace_end
END SUBROUTINE allocate_scalar

!*******************************************************************************
SUBROUTINE deallocate_scalar (s)
  implicit none
  real, dimension(:,:,:), pointer:: s
!...............................................................................
  deallocate (s)
END SUBROUTINE deallocate_scalar

!*******************************************************************************
FUNCTION ddx (m, a) RESULT (b)
  implicit none
  type(mesh_t):: m(3)
  real, dimension(:,:,:), pointer:: a, b
!...............................................................................
  b = 0.0
  call ddx_add (m, a, b, +1.0)
END FUNCTION ddx
!*******************************************************************************

SUBROUTINE ddx_add (m, a, b, sign)
  implicit none
  type(mesh_t):: m(3)
  real, dimension(:,:,:), pointer:: a, b
  integer :: idim, ix, iy, iz
  real    :: ax, bx, sign
!...............................................................................
  ax = 8. / (12.*m(1)%d); bx = -1. / (12.*m(1)%d)
  !
  if (m(1)%n > 1) then ! x-derivative only non-zero iff x-dimension larger than 1
    do iz=m(3)%li,m(3)%ui
    do iy=m(2)%li,m(2)%ui
    do ix=m(1)%li,m(1)%ui
      b(ix,iy,iz) = b(ix,iy,iz) + sign* &
                   (ax*(a(ix+1,iy,iz) - a(ix-1,iy,iz)) &
                  + bx*(a(ix+2,iy,iz) - a(ix-2,iy,iz)))                         ! d(vx)/dx
    end do
    end do
    end do
  endif
  call scalar_stats (m, a, 'ddx_add:in')
  call scalar_stats (m, b, 'ddx_add:out')
END SUBROUTINE ddx_add

!*******************************************************************************
FUNCTION ddy (m, a) RESULT (b)
  implicit none
  type(mesh_t):: m(3)
  real, dimension(:,:,:), pointer:: a, b
!...............................................................................
  b = 0.0
  call ddy_add (m, a, b, +1.0)
END FUNCTION ddy

!*******************************************************************************
SUBROUTINE ddy_add (m, a, b, sign)
  implicit none
  type(mesh_t):: m(3)
  real, dimension(:,:,:), pointer:: a, b
  integer :: idim, ix, iy, iz
  real    :: ay, by, sign
!...............................................................................
  ay = 8. / (12.*m(2)%d); by = -1. / (12.*m(2)%d)
  !
  if (m(2)%n > 1) then
    do iz=m(3)%li,m(3)%ui
    do iy=m(2)%li,m(2)%ui
    do ix=m(1)%li,m(1)%ui
      b(ix,iy,iz) = b(ix,iy,iz) + sign* &
                   (ay*(a(ix,iy+1,iz) - a(ix,iy-1,iz)) &
                  + by*(a(ix,iy+2,iz) - a(ix,iy-2,iz)))                         ! d(vy)/dy
    end do
    end do
    end do
  endif
  call scalar_stats (m, a, 'ddy_add:in')
  call scalar_stats (m, b, 'ddy_add:out')
END SUBROUTINE ddy_add

!*******************************************************************************
FUNCTION ddz (m, a) RESULT (b)
  implicit none
  type(mesh_t):: m(3)
  real, dimension(:,:,:), pointer:: a, b
!...............................................................................
  b = 0.0
  call ddz_add (m, a, b, +1.0)
END FUNCTION ddz

!*******************************************************************************
SUBROUTINE ddz_add (m, a, b, sign)
  implicit none
  type(mesh_t):: m(3)
  real, dimension(:,:,:), pointer:: a, b
  integer :: idim, ix, iy, iz
  real    :: az, bz, sign
!...............................................................................
  az = 8. / (12.*m(3)%d); bz = -1. / (12.*m(3)%d)
  !
  if (m(3)%n > 1) then
    do iz=m(3)%li,m(3)%ui
    do iy=m(2)%li,m(2)%ui
    do ix=m(1)%li,m(1)%ui
      b(ix,iy,iz) = b(ix,iy,iz) + sign* &
                   (az*(a(ix,iy,iz+1) - a(ix,iy,iz-1)) &
                  + bz*(a(ix,iy,iz+2) - a(ix,iy,iz-2)))                         ! d(vz)/dz
    end do
    end do
    end do
  endif
  call scalar_stats (m, a, 'ddz_add:in')
  call scalar_stats (m, b, 'ddz_add:out')
END SUBROUTINE ddz_add

!*******************************************************************************
SUBROUTINE scalar_stats (m, f, c)
  USE mpi_reduce
  USE io, only: do_debug
  implicit none
  type(mesh_t):: m(3)
  real:: mn, av, mx
  real, dimension(:,:,:):: f
  real, dimension(:,:,:), pointer:: interior
  character(len=*):: c
!...............................................................................
  if (.not.do_debug) return
  allocate (interior(m(1)%n, m(2)%n, m(3)%n))
  interior = f(m(1)%li:m(1)%ui, m(2)%li:m(2)%ui, m(3)%li:m(3)%ui)               ! skip ghost zones
  mn = minval(interior);            call min_real_mpi(mn)     ! global min
  av = sum(interior)/product(m%n);  call sum_real_mpi(av)     ! global average
  mx = maxval(interior);            call max_real_mpi(mx)     ! global max
  if (master) print '(1x,a11,3x,a,1p,3e11.3)', c, ' min, aver, max :', mn, av, mx
  deallocate (interior)
END SUBROUTINE scalar_stats

!*******************************************************************************
FUNCTION laplace (m, f, c)
  implicit none
  type(mesh_t):: m(3)
  real, dimension(:,:,:), pointer:: f
  real, dimension(m(1)%gn,m(2)%gn,m(3)%gn):: laplace
  integer :: idim, ix, iy, iz
  real    :: a, b, c
!...............................................................................
  laplace = 0.0
  !
  if (m(1)%n > 1) then 
    a = c/(m(1)%d)**2 ; b = 2.*c/(m(1)%d)**2
    do iz=m(3)%lb  ,m(3)%ub
    do iy=m(2)%lb  ,m(2)%ub
    do ix=m(1)%lb+1,m(1)%ub-1
      laplace(ix,iy,iz) = &
      laplace(ix,iy,iz) +  a*(f(ix+1,iy,iz) + f(ix-1,iy,iz)) - b*f(ix,iy,iz)
    end do
    end do
    end do
  endif

  if (m(2)%n > 1) then 
    a = c/(m(2)%d)**2 ; b = 2.*c/(m(2)%d)**2
    do iz=m(3)%lb  ,m(3)%ub
    do iy=m(2)%lb+1,m(2)%ub-1
    do ix=m(1)%lb  ,m(1)%ub
      laplace(ix,iy,iz) = &
      laplace(ix,iy,iz) +  a*(f(ix,iy+1,iz) + f(ix,iy-1,iz)) - b*f(ix,iy,iz)
    end do
    end do
    end do
  endif

  if (m(3)%n > 1) then 
    a = c/(m(3)%d)**2 ; b = 2.*c/(m(3)%d)**2
    do iz=m(3)%lb+1,m(3)%ub-1
    do iy=m(2)%lb  ,m(2)%ub
    do ix=m(1)%lb  ,m(1)%ub
      laplace(ix,iy,iz) = &
      laplace(ix,iy,iz) +  a*(f(ix,iy,iz+1) + f(ix,iy,iz-1)) - b*f(ix,iy,iz)
    end do
    end do
    end do
  endif
END FUNCTION laplace

!*******************************************************************************
END MODULE scalar_mod
